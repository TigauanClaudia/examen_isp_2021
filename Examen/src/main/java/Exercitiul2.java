import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercitiul2 extends JFrame {

    public JFrame frame;

    public Exercitiul2() {

        frame = new JFrame("Examen");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 500);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(new Color(255, 102, 178));

        JTextField textf1 = new JTextField();
        textf1.setBounds(200, 40, 200, 30);
        panel.add(textf1);

        JTextField textf2 = new JTextField();
        textf2.setBounds(200, 100, 200, 30);
        panel.add(textf2);

        JTextArea texta = new JTextArea();
        texta.setBounds(200, 150, 200, 30);
        panel.add(texta);

        JButton b = new JButton("calculeaza");
        b.setBounds(200, 250, 200, 50);
        panel.add(b);


        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = Integer.parseInt(textf1.getText());
                int y = Integer.parseInt(textf2.getText());

                int rez = x + y;

                texta.setText(rez + "");

            }
        });


        frame.add(panel);
        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public static void main(String[] args) {

        new Exercitiul2();
    }
}
